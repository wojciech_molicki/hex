#include "can_naive_win.h"

#define CZERWONY 'r'
#define NIEBIESKI 'b'
#define PUSTY ' '

bool is_game_over_for_color(hex plansza[11][11], stats &plansza_stats, char kolor)
{
	int rozmiar = plansza_stats.rozmiar;
	char wynik = PUSTY;

	if (kolor == CZERWONY)
	{
		for (int i = 0; i < rozmiar; i++)
		{
			szukaj_jednej_drogi(plansza, i, 0, rozmiar, CZERWONY, &wynik);
			wyzeruj_odwiedzenia(plansza, rozmiar);
			if (wynik != PUSTY)
				break;
		}
	}
	if (kolor == NIEBIESKI)
	{
		for (int i = 0; i < rozmiar; i++)
		{
			szukaj_jednej_drogi(plansza, 0, i, rozmiar, NIEBIESKI, &wynik);
			wyzeruj_odwiedzenia(plansza, rozmiar);
			if (wynik != PUSTY)
				break;
		}
	}
	return (wynik == kolor);
}

point2D kolor_moze_wygrac_w_jednym_vs_naiwny(hex plansza[11][11], stats &plansza_stats, char kolor)
{
	//zwraca wskaznik do pola ktore jest polem wygrywajacym, NULL dla niemozliwej wygranej
	// NIE CZYSCI KOLORU

	point2D result;
	result.X = -1; result.Y = -1;

	int rozmiar = plansza_stats.rozmiar;
	int wolne_pola = plansza_stats.hex_count - (plansza_stats.red_pawns + plansza_stats.blue_pawns);

	if (((kolor == CZERWONY) && (plansza_stats.red_pawns > plansza_stats.blue_pawns)) ||
		((kolor == NIEBIESKI) && (plansza_stats.red_pawns == plansza_stats.blue_pawns)))
	{
		if (wolne_pola < 2)
			return result;
	}

	for (int i = 0; i < rozmiar; i++)
	{
		for (int j = 0; j < rozmiar; j++)
		{
			if (plansza[i][j].kolor == PUSTY)
			{
				plansza[i][j].kolor = kolor;

				if (kolor == CZERWONY)
				{
					if (wygrana_czerwonego(plansza, plansza_stats))
					{
						//p->kolor = PUSTY;
						result.X = i;
						result.Y = j;
						return result;
					}
				}
				if (kolor == NIEBIESKI)
				{
					if (wygrana_niebieskiego(plansza, plansza_stats))
					{
						//p->kolor = PUSTY;
						result.X = i;
						result.Y = j;
						return result;
					}
				}

				plansza[i][j].kolor = PUSTY;
			}
		}
	}
	return result;
}

bool istnieje_niewygrywajacy_ruch(hex plansza[11][11], stats &plansza_stats, char kolor)
{
	//przeglada plansze i jesli znajdzie ruch ktory nie bedzie ruchem wygrywajacym, zwraca true

	int rozmiar = plansza_stats.rozmiar;

	for (int i = 0; i < rozmiar; i++)
	{
		for (int j = 0; j < rozmiar; j++)
		{
			if (plansza[i][j].kolor == PUSTY)
			{
				plansza[i][j].kolor = kolor;

				if (is_game_over_for_color(plansza, plansza_stats, kolor) == false)
				{
					plansza[i][j].kolor = PUSTY;
					return true;
				}

				plansza[i][j].kolor = PUSTY;
			}
		}
	}
	return false;
}

point2D istnieje_wygrywajacy_ruch(hex plansza[11][11], stats &plansza_stats, char kolor)
{
	//przeglada plansze i jesli znajdzie ruch ktory bedzie ruchem wygrywajacym, zwraca go

	point2D wynik;
	wynik.X = -1;
	wynik.Y = -1;

	int rozmiar = plansza_stats.rozmiar;

	for (int i = 0; i < rozmiar; i++)
	{
		for (int j = 0; j < rozmiar; j++)
		{
			if (plansza[i][j].kolor == PUSTY)
			{
				plansza[i][j].kolor = kolor;

				if (is_game_over_for_color(plansza, plansza_stats, kolor) == true)
				{
					plansza[i][j].kolor = PUSTY;
					wynik.X = i; wynik.Y = j;
					return wynik;
				}
				plansza[i][j].kolor = PUSTY;
			}
		}
	}
	return wynik;
}

bool istnieja_dwa_niewygrywajace_ruchy(hex plansza[11][11], stats &plansza_stats, char kolor)
{
	int wolne_pola = plansza_stats.hex_count - (plansza_stats.red_pawns + plansza_stats.blue_pawns);
	int rozmiar = plansza_stats.rozmiar;

	if (((kolor == CZERWONY) && (plansza_stats.red_pawns > plansza_stats.blue_pawns)) ||
		((kolor == NIEBIESKI) && (plansza_stats.red_pawns == plansza_stats.blue_pawns)))
	{
		if (wolne_pola < 4)
			return false;
	}
	for (int i = 0; i < rozmiar; i++)
	{
		for (int j = 0; j < rozmiar; j++)
		{
			if (plansza[i][j].kolor == PUSTY)
			{
				plansza[i][j].kolor = kolor;

				if (istnieje_niewygrywajacy_ruch(plansza, plansza_stats, kolor))
				{
					plansza[i][j].kolor = PUSTY;
					return true;
				}

				plansza[i][j].kolor = PUSTY;
			}
		}
	}
	return false;
}

bool wygrana_czerwonego(hex plansza[11][11], stats &plansza_stats)
{
	//TODO
	bool moze_wygrac = is_game_over_for_color(plansza, plansza_stats, CZERWONY);
	if (moze_wygrac && (plansza_stats.red_pawns == plansza_stats.blue_pawns))
	{
		return true;
	}
	else if (moze_wygrac && (plansza_stats.red_pawns > plansza_stats.blue_pawns))
	{
		// ale jesli ruch niebieskiego bylby wygrywajacy ?
		return istnieje_niewygrywajacy_ruch(plansza, plansza_stats, NIEBIESKI);
	}
	return false;
}

bool wygrana_niebieskiego(hex plansza[11][11], stats &plansza_stats)
{
	bool moze_wygrac = is_game_over_for_color(plansza, plansza_stats, NIEBIESKI);
	if (moze_wygrac && (plansza_stats.red_pawns == plansza_stats.blue_pawns))
	{
		// red -> blue
		return istnieje_niewygrywajacy_ruch(plansza, plansza_stats, CZERWONY);
	}
	else if (moze_wygrac && (plansza_stats.red_pawns > plansza_stats.blue_pawns))
	{
		// blue
		return true;
	}
	return false;
}

bool czerwony_moze_wygrac_w_dwoch_vs_naiwny(hex plansza[11][11], stats &plansza_stats)
{
	int czerwone = plansza_stats.red_pawns, niebieskie = plansza_stats.blue_pawns;
	int wolne_pola = plansza_stats.hex_count - (czerwone + niebieskie);

	if (wolne_pola < 3)
		return false;

	if ((czerwone > niebieskie) && (wolne_pola < 4))
		return false;

	int rozmiar = plansza_stats.rozmiar;

	for (int i = 0; i < rozmiar; i++)
	{
		for (int j = 0; j < rozmiar; j++)
		{
			if (plansza[i][j].kolor == PUSTY)
			{
				plansza[i][j].kolor = CZERWONY;

				//pierwszy ruch nie moze byc wygrywajacy
				if (is_game_over_for_color(plansza, plansza_stats, CZERWONY))
				{
					plansza[i][j].kolor = PUSTY;
					continue;
				}

				if (czerwone > niebieskie)
				{
					//niebieski -> czerwony -> niebieski -> czerwony
					if (wolne_pola < 4)
					{
						plansza[i][j].kolor = PUSTY;
						return false;
					}

					point2D wygrywajace_pole_drugie = kolor_moze_wygrac_w_jednym_vs_naiwny(plansza, plansza_stats, CZERWONY);

					//jesli istnieje wygrywajace pole dla czerwonego
					if (wygrywajace_pole_drugie.X != -1)
					{
						if (istnieja_dwa_niewygrywajace_ruchy(plansza, plansza_stats, NIEBIESKI))
						{
							plansza[i][j].kolor = PUSTY;
							plansza[wygrywajace_pole_drugie.X][wygrywajace_pole_drugie.Y].kolor = PUSTY;
							return true;
						}
						else
						{
							plansza[wygrywajace_pole_drugie.X][wygrywajace_pole_drugie.Y].kolor = PUSTY;
						}
					}
				}
				if (czerwone == niebieskie)
				{
					point2D wygrywajace_pole_drugie = kolor_moze_wygrac_w_jednym_vs_naiwny(plansza, plansza_stats, CZERWONY);
					if (wygrywajace_pole_drugie.X != -1)
					{
						if (istnieje_niewygrywajacy_ruch(plansza, plansza_stats, NIEBIESKI))
						{
							plansza[wygrywajace_pole_drugie.X][wygrywajace_pole_drugie.Y].kolor = PUSTY;
							plansza[i][j].kolor = PUSTY;
							return true;
						}
						else
						{
							plansza[wygrywajace_pole_drugie.X][wygrywajace_pole_drugie.Y].kolor = PUSTY;
						}
					}
				}
				plansza[i][j].kolor = PUSTY;
			}
		}
	}
	return false;
}

bool niebieski_moze_wygrac_w_dwoch_vs_naiwny(hex plansza[11][11], stats &plansza_stats)
{
	int czerwone = plansza_stats.red_pawns, niebieskie = plansza_stats.blue_pawns;
	int wolne_pola = plansza_stats.hex_count - (czerwone + niebieskie);

	if (wolne_pola < 3)
		return false;

	if ((czerwone == niebieskie) && (wolne_pola < 4))
		return false;

	int rozmiar = plansza_stats.rozmiar;

	for (int i = 0; i < rozmiar; i++)
	{
		for (int j = 0; j < rozmiar; j++)
		{
			if (plansza[i][j].kolor == PUSTY)
			{
				plansza[i][j].kolor = NIEBIESKI;

				//pierwszy ruch nie moze byc wygrywajacy
				if (is_game_over_for_color(plansza, plansza_stats, NIEBIESKI))
				{
					plansza[i][j].kolor = PUSTY;
					continue;
				}

				if (czerwone > niebieskie)
				{
					//niebieski -> czerwony -> niebieski

					//jesli istnieje wygrywajace pole dla czerwonego
					point2D wygrywajace_pole_drugie = kolor_moze_wygrac_w_jednym_vs_naiwny(plansza, plansza_stats, NIEBIESKI);
					if (wygrywajace_pole_drugie.X != -1)
					{
						if (istnieje_niewygrywajacy_ruch(plansza, plansza_stats, CZERWONY))
						{
							plansza[wygrywajace_pole_drugie.X][wygrywajace_pole_drugie.Y].kolor = PUSTY;
							plansza[i][j].kolor = PUSTY;
							return true;
						}
						else
						{
							plansza[wygrywajace_pole_drugie.X][wygrywajace_pole_drugie.Y].kolor = PUSTY;
						}
					}
				}
				if (czerwone == niebieskie)
				{
					point2D wygrywajace_pole_drugie = kolor_moze_wygrac_w_jednym_vs_naiwny(plansza, plansza_stats, NIEBIESKI);
					if (wygrywajace_pole_drugie.X != -1)
					{
						if (istnieja_dwa_niewygrywajace_ruchy(plansza, plansza_stats, CZERWONY))
						{
							plansza[i][j].kolor = PUSTY;
							plansza[wygrywajace_pole_drugie.X][wygrywajace_pole_drugie.Y].kolor = PUSTY;
							return true;
						}
					}
				}
				plansza[i][j].kolor = PUSTY;
			}
		}
	}
	return false;
}

bool czerwony_moze_wygrac_w_2_ruchach_vs_perfect(hex plansza[11][11], stats &plansza_stats)
{
	int czerwone = plansza_stats.red_pawns, niebieskie = plansza_stats.blue_pawns;
	int wolne_pola = plansza_stats.hex_count - (czerwone + niebieskie);

	if ((czerwone > niebieskie) && (wolne_pola < 4))
		return false;

	int rozmiar = plansza_stats.rozmiar;

	if (czerwone > niebieskie)
	{
		//jesli jest jeden wygrywajacy ruch dla niebieskiego, konczymy
		if (istnieje_wygrywajacy_ruch(plansza, plansza_stats, NIEBIESKI).X != -1)
			return false;

		// jesli istnieje wygrana dla czerwonego w jednym ruchu, to NIE BLOKUJ JEJ

		for (int i = 0; i < rozmiar; i++)
		{
			for (int j = 0; j < rozmiar; j++)
			{
				hex* polePtr = &plansza[i][j];
				if (polePtr->kolor != PUSTY)
					continue;

				//ruch niebieskiego, niebieski -> czerwony -> niebieski -> czerwony

				polePtr->kolor = NIEBIESKI;
				plansza_stats.blue_pawns++;
				if (czerwony_moze_wygrac_w_2_ruchach_vs_perfect(plansza, plansza_stats) == false)
				{
					polePtr->kolor = PUSTY;
					plansza_stats.blue_pawns--;
					return false;
				}
				plansza_stats.blue_pawns--;
				polePtr->kolor = PUSTY;
			}
		}
		//nie znalazlem
		return true;
	}
	if (niebieskie == czerwone)
	{
		// czyli ruch czerwonego -> niebieskiego -> czerwonego
		// pierwszy niewygrywajacy, niebieski tez niewygrywajacy, i czerwony wygrywajacy
		for (int i = 0; i < rozmiar; i++)
		{
			for (int j = 0; j < rozmiar; j++)
			{
				hex* polePtr = &plansza[i][j];
				if (polePtr->kolor != PUSTY)
					continue;

				polePtr->kolor = CZERWONY;
				if (is_game_over_for_color(plansza, plansza_stats, CZERWONY) == true)
				{
					polePtr->kolor = PUSTY;
					continue;
				}

				// wyszukaj drugie wygrywajace pole
				point2D drugie_wygrywajace_dla_czerwonego = istnieje_wygrywajacy_ruch(plansza, plansza_stats, CZERWONY);
				if (drugie_wygrywajace_dla_czerwonego.X != -1)
				{
					plansza[drugie_wygrywajace_dla_czerwonego.X][drugie_wygrywajace_dla_czerwonego.Y].kolor = NIEBIESKI;

					point2D trzeci_wygrywajacy = istnieje_wygrywajacy_ruch(plansza, plansza_stats, CZERWONY);
					if (trzeci_wygrywajacy.X != -1)
					{
						polePtr->kolor = PUSTY;
						plansza[drugie_wygrywajace_dla_czerwonego.X][drugie_wygrywajace_dla_czerwonego.Y].kolor = PUSTY;
						return true;
					}
					plansza[drugie_wygrywajace_dla_czerwonego.X][drugie_wygrywajace_dla_czerwonego.Y].kolor = PUSTY;
				}
				polePtr->kolor = PUSTY;
			}
		}
	}
	return false;
}

bool niebieski_moze_wygrac_w_2_ruchach_vs_perfect(hex plansza[11][11], stats &plansza_stats)
{
	int czerwone = plansza_stats.red_pawns, niebieskie = plansza_stats.blue_pawns;
	int wolne_pola = plansza_stats.hex_count - (czerwone + niebieskie);

	if ((czerwone == niebieskie) && (wolne_pola < 4))
		return false;

	int rozmiar = plansza_stats.rozmiar;

	//ruch czerwonego
	if (czerwone == niebieskie)
	{
		//jesli jest jeden wygrywajacy ruch dla czerwonego, konczymy
		if (istnieje_wygrywajacy_ruch(plansza, plansza_stats, CZERWONY).X != -1)
			return false;

		// jesli istnieje wygrana dla niebieskiego w jednym ruchu, to NIE BLOKUJ JEJ

		for (int i = 0; i < rozmiar; i++)
		{
			for (int j = 0; j < rozmiar; j++)
			{
				hex* polePtr = &plansza[i][j];
				if (polePtr->kolor != PUSTY)
					continue;

				//ruch czerwonego, czerwony -> niebieski -> czerwony -> niebieski

				polePtr->kolor = CZERWONY;
				plansza_stats.red_pawns++;
				if (niebieski_moze_wygrac_w_2_ruchach_vs_perfect(plansza, plansza_stats) == false)
				{
					polePtr->kolor = PUSTY;
					plansza_stats.red_pawns--;
					return false;
				}
				plansza_stats.red_pawns--;
				polePtr->kolor = PUSTY;
			}
		}
		//nie znalazlem
		return true;
	}
	if (niebieskie < czerwone)
	{
		// czyli ruch niebieskiego -> czerwonego -> niebieskiego
		// pierwszy niewygrywajacy, niebieski tez niewygrywajacy, i czerwony wygrywajacy
		for (int i = 0; i < rozmiar; i++)
		{
			for (int j = 0; j < rozmiar; j++)
			{
				hex* polePtr = &plansza[i][j];
				if (polePtr->kolor != PUSTY)
					continue;

				polePtr->kolor = NIEBIESKI;
				if (is_game_over_for_color(plansza, plansza_stats, NIEBIESKI) == true)
				{
					polePtr->kolor = PUSTY;
					continue;
				}

				// wyszukaj drugie wygrywajace pole
				point2D drugie_wygrywajace_dla_niebieskiego = istnieje_wygrywajacy_ruch(plansza, plansza_stats, NIEBIESKI);
				if (drugie_wygrywajace_dla_niebieskiego.X != -1)
				{
					//blokuj czerwonym wygrywajacy ruch
					plansza[drugie_wygrywajace_dla_niebieskiego.X][drugie_wygrywajace_dla_niebieskiego.Y].kolor = CZERWONY;

					point2D trzeci_wygrywajacy = istnieje_wygrywajacy_ruch(plansza, plansza_stats, NIEBIESKI);
					if (trzeci_wygrywajacy.X != -1)
					{
						polePtr->kolor = PUSTY;
						plansza[drugie_wygrywajace_dla_niebieskiego.X][drugie_wygrywajace_dla_niebieskiego.Y].kolor = PUSTY;
						return true;
					}
					plansza[drugie_wygrywajace_dla_niebieskiego.X][drugie_wygrywajace_dla_niebieskiego.Y].kolor = PUSTY;
				}
				polePtr->kolor = PUSTY;
			}
		}
	}
	return false;
}

point2D niebieski_moze_wygrac_w_jednym_vs_perfect(hex plansza[11][11], stats &plansza_stats)
{
	//zwraca pole ktore jest polem wygrywajacym, wspolrzedne -1,-1 dla niemozliwej wygranej
	// NIE CZYSCI KOLORU

	point2D result;
	result.X = -1; result.Y = -1;

	int rozmiar = plansza_stats.rozmiar;
	int wolne_pola = plansza_stats.hex_count - (plansza_stats.red_pawns + plansza_stats.blue_pawns);

	if (plansza_stats.red_pawns == plansza_stats.blue_pawns && wolne_pola < 2)
		return result;

	for (int i = 0; i < rozmiar; i++)
	{
		for (int j = 0; j < rozmiar; j++)
		{
			hex* polePtr = &plansza[i][j];
			if (polePtr->kolor == PUSTY)
			{
				polePtr->kolor = NIEBIESKI;

				//czyli ruch czerwonego	-> czerwony musi sie ruszyc w wygrywajace pole niebieskiego (jesli jest)
				if (plansza_stats.red_pawns == plansza_stats.blue_pawns)
				{
					if (wygrana_niebieskiego(plansza, plansza_stats))
					{
						polePtr->kolor = CZERWONY;
						plansza_stats.red_pawns++;

						point2D perfect_win = kolor_moze_wygrac_w_jednym_vs_naiwny(plansza, plansza_stats, NIEBIESKI);

						polePtr->kolor = PUSTY;
						plansza_stats.red_pawns--;
						return perfect_win;
					}
				}
				// czyli ruch niebieskiego
				else {
					if (wygrana_niebieskiego(plansza, plansza_stats))
					{
						result.X = i;
						result.Y = j;
						return result;
					}
				}
				polePtr->kolor = PUSTY;
			}
		}
	}
	return result;
}

point2D czerwony_moze_wygrac_w_jednym_vs_perfect(hex plansza[11][11], stats &plansza_stats)
{
	//zwraca wskaznik do pola ktore jest polem wygrywajacym, NULL dla niemozliwej wygranej
	// NIE CZYSCI KOLORU

	point2D result;
	result.X = -1; result.Y = -1;

	int rozmiar = plansza_stats.rozmiar;
	int wolne_pola = plansza_stats.hex_count - (plansza_stats.red_pawns + plansza_stats.blue_pawns);

	if (plansza_stats.red_pawns > plansza_stats.blue_pawns && wolne_pola < 2)
		return result;

	for (int i = 0; i < rozmiar; i++)
	{
		for (int j = 0; j < rozmiar; j++)
		{
			hex* polePtr = &plansza[i][j];
			if (polePtr->kolor == PUSTY)
			{
				polePtr->kolor = CZERWONY;

				//czyli ruch czerwonego
				if (plansza_stats.red_pawns == plansza_stats.blue_pawns)
				{
					if (wygrana_czerwonego(plansza, plansza_stats))
					{
						result.X = i;
						result.Y = j;
						return result;
					}
				}
				// czyli ruch niebieskiego -> niebieski musi sie ruszyc w wygrywajace pole czerwonego (jesli jest)
				else
				{
					if (wygrana_czerwonego(plansza, plansza_stats))
					{
						polePtr->kolor = NIEBIESKI;
						plansza_stats.blue_pawns++;

						point2D perfect_win = kolor_moze_wygrac_w_jednym_vs_naiwny(plansza, plansza_stats, CZERWONY);

						polePtr->kolor = PUSTY;
						plansza_stats.blue_pawns--;
						return perfect_win;
					}
				}
				polePtr->kolor = PUSTY;
			}
		}
	}
	return result;
}