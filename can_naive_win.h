#ifndef CAN_NAIVE_WIN_H
#define CAN_NAIVE_WIN_H

#include "dzialania_na_mapie.h"

struct point2D {
	int X;
	int Y;
};

bool wygrana_czerwonego(hex plansza[11][11], stats &plansza_stats);
bool wygrana_niebieskiego(hex plansza[11][11], stats &plansza_stats);

bool istnieje_niewygrywajacy_ruch(hex plansza[11][11], stats &plansza_stats, char kolor);
bool istnieja_dwa_niewygrywajace_ruchy(hex plansza[11][11], stats &plansza_stats, char kolor);

bool is_game_over_for_color(hex plansza[11][11], stats &plansza_stats, char kolor);

point2D kolor_moze_wygrac_w_jednym_vs_naiwny(hex plansza[11][11], stats &plansza_stats, char kolor);

point2D czerwony_moze_wygrac_w_jednym_vs_perfect(hex plansza[11][11], stats &plansza_stats);
point2D niebieski_moze_wygrac_w_jednym_vs_perfect(hex plansza[11][11], stats &plansza_stats);

bool czerwony_moze_wygrac_w_dwoch_vs_naiwny(hex plansza[11][11], stats &plansza_stats);
bool niebieski_moze_wygrac_w_dwoch_vs_naiwny(hex plansza[11][11], stats &plansza_stats);

bool czerwony_moze_wygrac_w_2_ruchach_vs_perfect(hex plansza[11][11], stats &plansza_stats);
bool niebieski_moze_wygrac_w_2_ruchach_vs_perfect(hex plansza[11][11], stats &plansza_stats);

#endif