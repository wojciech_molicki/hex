#include "dzialania_na_mapie.h"

#include "can_naive_win.h"
#include "is_board_possible.h"

#define CZERWONY 'r'
#define NIEBIESKI 'b'
#define PUSTY ' '

void delete_newline_eof(char *bufor)
{
	//usuniecie nowej linii i eof z bufora
	char *pos;
	if ((pos = strchr(bufor, '\n')) != NULL)
		*pos = '\0';
}

void hex_init(hex &h)
{
	h.kolor = PUSTY;
	h.odwiedzony = false;
	h.numer_wezla = -1;
}

void sprawdz_wygrana(hex plansza[11][11], stats &plansza_stats) {
	char wynik;
	is_game_over(plansza, plansza_stats, &wynik);

	switch (wynik)
	{
	case CZERWONY:
		printf("YES RED\n");
		return;
		break;
	case NIEBIESKI:
		printf("YES BLUE\n");
		return;
		break;
	default:
		printf("NO\n");
		return;
		break;
	};
}

bool is_game_over(hex plansza[11][11], stats &plansza_stats, char *wygrany)
{
	char wynik = PUSTY;
	int rozmiar = plansza_stats.rozmiar;

	//optymalizacja
	if (plansza_stats.red_pawns >= plansza_stats.blue_pawns)
	{
		for (int i = 0; i < rozmiar; i++)
		{
			szukaj_jednej_drogi(plansza, i, 0, plansza_stats.rozmiar, CZERWONY, &wynik);
			wyzeruj_odwiedzenia(plansza, rozmiar);
			if (wynik != PUSTY)
				break;
		}

		if (wynik == PUSTY)
		{
			wyzeruj_odwiedzenia(plansza, rozmiar);
			for (int i = 0; i < rozmiar; i++)
			{
				szukaj_jednej_drogi(plansza, 0, i, plansza_stats.rozmiar, NIEBIESKI, &wynik);
				wyzeruj_odwiedzenia(plansza, rozmiar);
				if (wynik != PUSTY)
					break;
			}
		}
	}
	else
	{
		for (int i = 0; i < rozmiar; i++)
		{
			szukaj_jednej_drogi(plansza, 0, i, plansza_stats.rozmiar, NIEBIESKI, &wynik);
			wyzeruj_odwiedzenia(plansza, rozmiar);
			if (wynik != PUSTY)
				break;
		}

		if (wynik == PUSTY)
		{
			wyzeruj_odwiedzenia(plansza, rozmiar);
			for (int i = 0; i < rozmiar; i++)
			{
				szukaj_jednej_drogi(plansza, i, 0, plansza_stats.rozmiar, CZERWONY, &wynik);
				wyzeruj_odwiedzenia(plansza, rozmiar);
				if (wynik != PUSTY)
					break;
			}
		}
	}
	*wygrany = wynik;
	return (wynik != PUSTY);
}

void szukaj_jednej_drogi(hex plansza[11][11], int poleX, int poleY, int rozmiar, char kolor_gracza, char *wynik)
{
	if (*wynik != PUSTY)
		return;

	hex *pole = &plansza[poleX][poleY];
	pole->odwiedzony = true;

	if (pole->kolor != kolor_gracza)
		return;

	if ((kolor_gracza == CZERWONY) && (poleY == rozmiar-1))
	{
		*wynik = CZERWONY;
		return;
	}
	if ((kolor_gracza == NIEBIESKI) && (poleX == rozmiar-1))
	{
		*wynik = NIEBIESKI;
		return;
	}

	//DOL
	if ((poleX+1 < rozmiar && poleY+1 < rozmiar) && (plansza[poleX+1][poleY+1].kolor == kolor_gracza) &&
		(plansza[poleX+1][poleY+1].odwiedzony == false)) {
			szukaj_jednej_drogi(plansza, poleX+1, poleY+1, rozmiar, kolor_gracza, wynik);
	}

	//GORA
	if ((poleX-1 >= 0) && (poleY-1 >= 0) && (plansza[poleX-1][poleY-1].kolor == kolor_gracza) &&
		(plansza[poleX-1][poleY-1].odwiedzony == false)) {
			szukaj_jednej_drogi(plansza, poleX-1, poleY-1, rozmiar, kolor_gracza, wynik);
	}

	//LEWO_GORA
	if ((poleY-1 >= 0) && (plansza[poleX][poleY-1].kolor == kolor_gracza) && (plansza[poleX][poleY-1].odwiedzony == false)) {
		szukaj_jednej_drogi(plansza, poleX, poleY-1, rozmiar, kolor_gracza, wynik);
	}

	//PRAWO_GORA
	if ((poleX-1 >= 0) && (plansza[poleX-1][poleY].kolor == kolor_gracza) && (plansza[poleX-1][poleY].odwiedzony == false)) {
		szukaj_jednej_drogi(plansza, poleX-1, poleY, rozmiar, kolor_gracza, wynik);
	}

	//LEWO_DOL
	if ((poleX+1 < rozmiar) && (plansza[poleX+1][poleY].kolor == kolor_gracza) && (plansza[poleX+1][poleY].odwiedzony == false)) {
		szukaj_jednej_drogi(plansza, poleX+1, poleY, rozmiar, kolor_gracza, wynik);
	}

	//PRAWO_DOL
	if ((poleY+1 < rozmiar) && (plansza[poleX][poleY+1].kolor == kolor_gracza) && (plansza[poleX][poleY+1].odwiedzony == false)) {
		szukaj_jednej_drogi(plansza, poleX, poleY+1, rozmiar, kolor_gracza, wynik);
	}
}

void wyzeruj_odwiedzenia(hex plansza[11][11], int rozmiar)
{
	for (int i = 0; i < rozmiar; i++)
	{
		for (int j = 0; j < rozmiar; j++)
			plansza[i][j].odwiedzony = false;
	}
}

bool is_board_correct(stats &plansza_stats) {
	return (abs(plansza_stats.blue_pawns - plansza_stats.red_pawns) <= 1 &&
		plansza_stats.blue_pawns <= plansza_stats.red_pawns);
}

void obsluz_zapytanie(char* bufor, hex plansza[11][11], stats &plansza_stats) {
	if (strcmp(bufor,"BOARD_SIZE") == 0) {
		printf("%d\n", plansza_stats.rozmiar);
		return;
	}
	else if (strcmp(bufor,"PAWNS_NUMBER") == 0) {
		printf("%d\n", plansza_stats.blue_pawns + plansza_stats.red_pawns);
		return;
	}
	else if (strcmp(bufor,"IS_BOARD_CORRECT") == 0) {
		if (is_board_correct(plansza_stats) == true)
			printf("YES\n");
		else
			printf("NO\n");
		return;
	}
	else if (strcmp(bufor,"IS_GAME_OVER") == 0) {
		if (is_board_correct(plansza_stats) == false)
			printf("NO\n");
		else
			sprawdz_wygrana(plansza, plansza_stats);
		return;
	}
	else if (strcmp(bufor,"IS_BOARD_POSSIBLE") == 0) {
		if (is_board_correct(plansza_stats) == false)
			printf("NO\n");
		else
			sprawdz_possible(plansza, plansza_stats);
		return;
	}

	int wolne_pola =  plansza_stats.hex_count - (plansza_stats.red_pawns + plansza_stats.blue_pawns);

	if (bufor[0] == 'C')
	{
		if (wolne_pola < 1)
		{
			printf("NO\n");
			return;
		}
		if ((is_board_correct(plansza_stats) == false) || is_game_over_for_color(plansza, plansza_stats, CZERWONY) || is_game_over_for_color(plansza, plansza_stats, NIEBIESKI))
		{
			printf("NO\n");
			return;
		}
	}

	if (strcmp(bufor,"CAN_RED_WIN_IN_1_MOVE_WITH_NAIVE_OPPONENT") == 0) {
		if (plansza_stats.red_pawns < plansza_stats.rozmiar - 1)
		{
			printf("NO\n");
			return;
		}
		point2D pole_wygrywajace = kolor_moze_wygrac_w_jednym_vs_naiwny(plansza, plansza_stats, CZERWONY);
		if (pole_wygrywajace.X == -1)
			printf("NO\n");
		else
		{
			plansza[pole_wygrywajace.X][pole_wygrywajace.Y].kolor = PUSTY;
			printf("YES\n");
		}
		return;
	}
	else if (strcmp(bufor,"CAN_BLUE_WIN_IN_1_MOVE_WITH_NAIVE_OPPONENT") == 0) {
		if (plansza_stats.blue_pawns < plansza_stats.rozmiar - 1)
		{
			printf("NO\n");
			return;
		}
		point2D pole_wygrywajace = kolor_moze_wygrac_w_jednym_vs_naiwny(plansza, plansza_stats, NIEBIESKI);
		if (pole_wygrywajace.X == -1)
			printf("NO\n");
		else
		{
			plansza[pole_wygrywajace.X][pole_wygrywajace.Y].kolor = PUSTY;
			printf("YES\n");
		}
		return;
	}

	if (strcmp(bufor,"CAN_RED_WIN_IN_1_MOVE_WITH_PERFECT_OPPONENT") == 0) {
		if (plansza_stats.red_pawns < plansza_stats.rozmiar - 1)
		{
			printf("NO\n");
			return;
		}
		point2D pole_wygrywajace = czerwony_moze_wygrac_w_jednym_vs_perfect(plansza, plansza_stats);
		if (pole_wygrywajace.X == -1)
			printf("NO\n");
		else
		{
			plansza[pole_wygrywajace.X][pole_wygrywajace.Y].kolor = PUSTY;
			printf("YES\n");
		}
		return;
	}

	else if (strcmp(bufor,"CAN_BLUE_WIN_IN_1_MOVE_WITH_PERFECT_OPPONENT") == 0) {
		if (plansza_stats.blue_pawns < plansza_stats.rozmiar - 1)
		{
			printf("NO\n");
			return;
		}
		point2D pole_wygrywajace = niebieski_moze_wygrac_w_jednym_vs_perfect(plansza, plansza_stats);
		if (pole_wygrywajace.X == -1)
			printf("NO\n");
		else
		{
			plansza[pole_wygrywajace.X][pole_wygrywajace.Y].kolor = PUSTY;
			printf("YES\n");
		}
		return;
	}

	if ((bufor[0] == 'C') && (wolne_pola < 3))
	{
		printf("NO\n");
		return;
	}

	if (strcmp(bufor,"CAN_RED_WIN_IN_2_MOVES_WITH_NAIVE_OPPONENT") == 0) {
		if (plansza_stats.red_pawns < plansza_stats.rozmiar - 2)
		{
			printf("NO\n");
			return;
		}
		if (czerwony_moze_wygrac_w_dwoch_vs_naiwny(plansza, plansza_stats) == false)
			printf("NO\n");
		else
			printf("YES\n");
		return;
	}
	else if (strcmp(bufor,"CAN_BLUE_WIN_IN_2_MOVES_WITH_NAIVE_OPPONENT") == 0) {
		if (plansza_stats.blue_pawns < plansza_stats.rozmiar - 2)
		{
			printf("NO\n");
			return;
		}
		if (niebieski_moze_wygrac_w_dwoch_vs_naiwny(plansza, plansza_stats) == false)
			printf("NO\n");
		else
			printf("YES\n");
		return;
	}

	else if (strcmp(bufor,"CAN_RED_WIN_IN_2_MOVES_WITH_PERFECT_OPPONENT") == 0) {
		if (plansza_stats.red_pawns < plansza_stats.rozmiar - 2)
		{
			printf("NO\n");
			return;
		}
		if (czerwony_moze_wygrac_w_2_ruchach_vs_perfect(plansza, plansza_stats) == false)
			printf("NO\n");
		else
			printf("YES\n");
		return;
	}
	else if (strcmp(bufor,"CAN_BLUE_WIN_IN_2_MOVES_WITH_PERFECT_OPPONENT") == 0) {
		if (plansza_stats.blue_pawns < plansza_stats.rozmiar - 2)
		{
			printf("NO\n");
			return;
		}
		if (niebieski_moze_wygrac_w_2_ruchach_vs_perfect(plansza, plansza_stats) == false)
			printf("NO\n");
		else
			printf("YES\n");
		return;
	}
}

stats wczytaj_plansze(hex plansza[11][11]) {
	stats wynik;

	char znak, poprzedni_znak = ' ';
	int hex_count = 0;
	int poziom_zwezenia = 1;
	bool zwezaj = false;

	int lewa = 0, prawa = 0;
	int max = -1;

	while(znak = getchar())
	{
		if (znak == EOF)
			break;
		if (poprzedni_znak == '>' && znak == '\n')
			zwezaj = true;
		poprzedni_znak = znak;

		if (znak == '<')
		{
			hex_count++;
			//zjedzenie spacji
			znak = getchar();

			znak = getchar();

			if (znak == CZERWONY)
				wynik.red_pawns++;
			if (znak == NIEBIESKI)
				wynik.blue_pawns++;

			lewa--;
			prawa++;

			if (zwezaj == false)
			{
				if (lewa < 0)
				{
					max++;
					wynik.rozmiar++;
					lewa = max;
					prawa = 0;
				}
			}

			if (zwezaj == true)
			{
				if (prawa > max)
				{
					prawa = poziom_zwezenia;
					poziom_zwezenia++;
					lewa = max;
				}
			}

			hex_init(plansza[lewa][prawa]);

			plansza[lewa][prawa].numer_wezla = hex_count;
			plansza[lewa][prawa].kolor = znak;
		}

		if (znak >= 'A' && znak <= 'Z') {
			ungetc(znak, stdin);
			break;
		}
	}
	wynik.hex_count = hex_count;
	return wynik;
}