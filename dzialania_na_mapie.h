#ifndef DZIALANIA_NA_MAPIE_H
#define DZIALANIA_NA_MAPIE_H

#include <cstring>
#include <iostream>

typedef struct hex {
	int numer_wezla;
	char kolor;
	bool odwiedzony;
} hex;

struct stats {
	int hex_count;
	int red_pawns;
	int blue_pawns;
	int rozmiar;
	stats() : hex_count(0), red_pawns(0), blue_pawns(0), rozmiar(0) {}
};

void delete_newline_eof(char *bufor);
void obsluz_zapytanie(char* bufor, hex tablica[11][11], stats &plansza_stats);

void szukaj_jednej_drogi(hex plansza[11][11], int poleX, int poleY, int rozmiar, char kolor_gracza, char *wynik);
void sprawdz_wygrana(hex plansza[11][11], stats &plansza_stats);
void wyzeruj_odwiedzenia(hex plansza[11][11], int rozmiar);

bool is_board_correct(stats &plansza_stats);
bool is_game_over(hex plansza[11][11], stats &plansza_stats, char *kto_wygral);

/////////////////////////////////////////////

stats wczytaj_plansze(hex tablica[11][11]);

#endif
