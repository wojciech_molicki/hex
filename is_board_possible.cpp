#include "is_board_possible.h"

#define CZERWONY 'r'
#define NIEBIESKI 'b'
#define PUSTY ' '

void sprawdz_possible(hex plansza[11][11], stats &plansza_stats)
{
	char wynik = PUSTY;
	int ile_sciezek_r = 0, ile_sciezek_b = 0;
	int rozmiar = plansza_stats.rozmiar;

	for (int i = 0; i < rozmiar; i++)
	{
		szukaj_drogi(plansza, i, 0, rozmiar, CZERWONY, &wynik, &ile_sciezek_r, &ile_sciezek_b);

		wyzeruj_odwiedzenia(plansza, rozmiar);
		wynik = PUSTY;
	}

	wyzeruj_odwiedzenia(plansza, rozmiar);

	for (int i = 0; i < plansza_stats.rozmiar; i++)
	{
		szukaj_drogi(plansza, 0, i, rozmiar, NIEBIESKI, &wynik, &ile_sciezek_r, &ile_sciezek_b);

		wyzeruj_odwiedzenia(plansza, rozmiar);
		wynik = PUSTY;
	}

	if (ile_sciezek_r + ile_sciezek_b <= 1)
	{
		if ((ile_sciezek_b == 1) && (plansza_stats.red_pawns != plansza_stats.blue_pawns))
		{
			printf("NO\n");
			return;
		}
		if ((ile_sciezek_r == 1) && (plansza_stats.red_pawns - plansza_stats.blue_pawns != 1 ))
		{
			printf("NO\n");
			return;
		}
		printf("YES\n");
		return;
	}
	if (ile_sciezek_b == 0)
	{
		if (czy_game_over_i_nadmiarowy_kamien(plansza, plansza_stats, CZERWONY) == true)
		{
			if (plansza_stats.red_pawns - 1 != plansza_stats.blue_pawns)
			{
				printf("NO\n");
				return;
			}
			printf("YES\n");
			return;
		}
	}
	if (ile_sciezek_r == 0)
	{
		if (czy_game_over_i_nadmiarowy_kamien(plansza, plansza_stats, NIEBIESKI) == true)
		{
			if (plansza_stats.red_pawns - plansza_stats.blue_pawns - 1 >= 0)
			{
				printf("NO\n");
				return;
			}
			printf("YES\n");
			return;
		}
	}

	printf("NO\n");
}

bool czy_game_over_i_nadmiarowy_kamien(hex plansza[11][11], stats &plansza_stats, char kolor)
{
	//sprawdza poprzez usuniecie jednego z kamieni danego koloru (dla wszystkich kamieni na planszy)
	//czy is_game_over jest ciagle prawdziwe

	int rozmiar = plansza_stats.rozmiar;
	char wynik;

	for (int i = 0; i < rozmiar; i++)
	{
		for (int j = 0; j < rozmiar; j++)
		{
			if (plansza[i][j].kolor == kolor)
			{
				plansza[i][j].kolor = PUSTY;
				if (is_game_over(plansza, plansza_stats, &wynik) == false)
					return true;
				plansza[i][j].kolor = kolor;
			}
		}
	}
	return false;
}

void szukaj_drogi(hex plansza[11][11], int poleX, int poleY, int rozmiar, char kolor_gracza,
				  char *wynik, int *ile_sciezek_r, int *ile_sciezek_b)
{
	hex *pole = &plansza[poleX][poleY];
	pole->odwiedzony = true;

	if (pole->kolor != kolor_gracza)
		return;

	if ((kolor_gracza == CZERWONY) && (poleY == rozmiar-1))
	{
		*wynik = CZERWONY;
		(*ile_sciezek_r)++;
		return;
	}
	if ((kolor_gracza == NIEBIESKI) && (poleX == rozmiar-1))
	{
		*wynik = NIEBIESKI;
		(*ile_sciezek_b)++;
		return;
	}

	//DOL
	if ((poleX+1 < rozmiar && poleY+1 < rozmiar) && (plansza[poleX+1][poleY+1].kolor == kolor_gracza) && (plansza[poleX+1][poleY+1].odwiedzony == false)) {
		szukaj_drogi(plansza, poleX+1, poleY+1, rozmiar, kolor_gracza, wynik, ile_sciezek_r, ile_sciezek_b);
	}

	//GORA
	if ((poleX-1 >= 0) && (poleY-1 >= 0) && (plansza[poleX-1][poleY-1].kolor == kolor_gracza) && (plansza[poleX-1][poleY-1].odwiedzony == false)) {
		szukaj_drogi(plansza, poleX-1, poleY-1, rozmiar, kolor_gracza, wynik, ile_sciezek_r, ile_sciezek_b);
	}

	//LEWO_GORA
	if ((poleY-1 >= 0) && (plansza[poleX][poleY-1].kolor == kolor_gracza) && (plansza[poleX][poleY-1].odwiedzony == false)) {
		szukaj_drogi(plansza, poleX, poleY-1, rozmiar, kolor_gracza, wynik, ile_sciezek_r, ile_sciezek_b);
	}

	//PRAWO_GORA
	if ((poleX-1 >= 0) && (plansza[poleX-1][poleY].kolor == kolor_gracza) && (plansza[poleX-1][poleY].odwiedzony == false)) {
		szukaj_drogi(plansza, poleX-1, poleY, rozmiar, kolor_gracza, wynik, ile_sciezek_r, ile_sciezek_b);
	}

	//LEWO_DOL
	if ((poleX+1 < rozmiar) && (plansza[poleX+1][poleY].kolor == kolor_gracza) && (plansza[poleX+1][poleY].odwiedzony == false)) {
		szukaj_drogi(plansza, poleX+1, poleY, rozmiar, kolor_gracza, wynik, ile_sciezek_r, ile_sciezek_b);
	}

	//PRAWO_DOL
	if ((poleY+1 < rozmiar) && (plansza[poleX][poleY+1].kolor == kolor_gracza) && (plansza[poleX][poleY+1].odwiedzony == false)) {
		szukaj_drogi(plansza, poleX, poleY+1, rozmiar, kolor_gracza, wynik, ile_sciezek_r, ile_sciezek_b);
	}
}