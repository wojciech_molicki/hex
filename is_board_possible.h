#ifndef IS_BOARD_POSSIBLE_H
#define IS_BOARD_POSSIBLE_H

#include "dzialania_na_mapie.h"

void sprawdz_possible(hex plansza[11][11], stats &plansza_stats);
void szukaj_drogi(hex plansza[11][11], int poleX, int poleY, int rozmiar, char kolor_gracza,
				  char *wynik, int *ile_sciezek_r, int *ile_sciezek_b);

bool czy_game_over_i_nadmiarowy_kamien(hex plansza[11][11], stats &plansza_stats, char kolor);

#endif