#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <cstring>

#include "dzialania_na_mapie.h"

#define SIZE 100

#define CZERWONY 'r'
#define NIEBIESKI 'b'
#define PUSTY ' '

int main()
{
	char bufor[SIZE];
	stats plansza_stats;
	hex plansza[11][11];

	bool pierwsza_plansza = true;

	while (fgets(bufor, SIZE, stdin) != NULL)
	{
		if (bufor[0] == ' ')
		{
			//dodaj newline
			if (pierwsza_plansza == false)
				printf("\n");

			plansza_stats = wczytaj_plansze(plansza);

			pierwsza_plansza = false;
		}
		else
		{
			delete_newline_eof(bufor);
			obsluz_zapytanie(bufor, plansza, plansza_stats);
		}
	}

	return 0;
}